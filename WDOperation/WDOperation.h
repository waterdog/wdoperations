//
//  WDOperation.h
//  WDOperation
//
//  Created by Jorge Galrito on 06/03/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WDOperation.
FOUNDATION_EXPORT double WDOperationVersionNumber;

//! Project version string for WDOperation.
FOUNDATION_EXPORT const unsigned char WDOperationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WDOperation/PublicHeader.h>


