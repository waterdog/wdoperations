/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This file shows an example of implementing the OperationCondition protocol.
*/

#if os(iOS)

import UIKit

/**
    A condition for verifying that we can present alerts to the user via
    `UILocalNotification` and/or remote notifications.
*/
public struct WDUserNotificationCondition: WDOperationCondition {
    
    public enum Behavior {
        /// Merge the new `UIUserNotificationSettings` with the `currentUserNotificationSettings`.
        case merge

        /// Replace the `currentUserNotificationSettings` with the new `UIUserNotificationSettings`.
        case replace
    }
    
    public static let name = "UserNotification"
    public static let currentSettings = "CurrentUserNotificationSettings"
    public static let desiredSettings = "DesiredUserNotificationSettigns"
    public static let isMutuallyExclusive = false
    
    public let settings: UIUserNotificationSettings
    public let application: UIApplication
    public let behavior: Behavior
    
    /**
        The designated initializer.
        
        - parameter settings: The `UIUserNotificationSettings` you wish to be
            registered.

        - parameter application: The `UIApplication` on which the `settings` should
            be registered.

        - parameter behavior: The way in which the `settings` should be applied
            to the `application`. By default, this value is `.Merge`, which means
            that the `settings` will be combined with the existing settings on the
            `application`. You may also specify `.Replace`, which means the `settings`
            will overwrite the exisiting settings.
    */
    public init(settings: UIUserNotificationSettings, application: UIApplication, behavior: Behavior = .merge) {
        self.settings = settings
        self.application = application
        self.behavior = behavior
    }
    
    public func dependencyForOperation(_ operation: WDOperation) -> Operation? {
        return UserNotificationPermissionOperation(settings: settings, application: application, behavior: behavior)
    }
    
    public func evaluateForOperation(_ operation: WDOperation, completion: @escaping (WDOperationConditionResult) -> Void) {
        let result: WDOperationConditionResult
        
        let current = application.currentUserNotificationSettings

        switch (current, settings)  {
        case (let current?, let settings) where current.contains(settings: settings):
            result = .satisfied
            
        default:
            let error = NSError(code: .conditionFailed, userInfo: [
                WDOperationConditionKey: type(of: self).name,
                type(of: self).currentSettings: current ?? NSNull(),
                type(of: self).desiredSettings: settings
                ])
            
            result = .failed(error)
        }
        
        completion(result)
    }
}

/**
    A private `Operation` subclass to register a `UIUserNotificationSettings`
    object with a `UIApplication`, prompting the user for permission if necessary.
*/
private class UserNotificationPermissionOperation: WDOperation {
    let settings: UIUserNotificationSettings
    let application: UIApplication
    let behavior: WDUserNotificationCondition.Behavior
    
    init(settings: UIUserNotificationSettings, application: UIApplication, behavior: WDUserNotificationCondition.Behavior) {
        self.settings = settings
        self.application = application
        self.behavior = behavior
        
        super.init()

        addCondition(WDAlertPresentation())
    }
    
    override func execute() {
        DispatchQueue.main.async {
            let current = self.application.currentUserNotificationSettings
            
            let settingsToRegister: UIUserNotificationSettings
            
            switch (current, self.behavior) {
            case (let currentSettings?, .merge):
                settingsToRegister = currentSettings.settingsByMerging(settings: self.settings)
                
            default:
                settingsToRegister = self.settings
            }
            
            self.application.registerUserNotificationSettings(settingsToRegister)
        }
    }
}
    
#endif
