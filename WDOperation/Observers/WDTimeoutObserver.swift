/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This file shows how to implement the OperationObserver protocol.
*/

import Foundation

/**
    `TimeoutObserver` is a way to make an `Operation` automatically time out and
    cancel after a specified time interval.
*/
public struct WDTimeoutObserver: WDOperationObserver {
    // MARK: Properties

    public static let timeoutKey = "Timeout"
    
    private let timeout: TimeInterval
    
    // MARK: Initialization
    
    public init(timeout: TimeInterval) {
        self.timeout = timeout
    }
    
    // MARK: OperationObserver
    
    public func operationDidStart(_ operation: WDOperation) {
        // When the operation starts, queue up a block to cause it to time out.
        let when = DispatchTime(uptimeNanoseconds: DispatchTime.now().uptimeNanoseconds + UInt64(timeout * Double(NSEC_PER_SEC)))

        DispatchQueue.global().asyncAfter(deadline: when) {
            /*
             Cancel the operation if it hasn't finished and hasn't already
             been cancelled.
             */
            if !operation.isFinished && !operation.isCancelled {
                let error = NSError(code: .executionFailed, userInfo: [
                    type(of: self).timeoutKey: self.timeout
                    ])
                
                operation.cancelWithError(error: error)
            }
        }
    }

    public func operation(_ operation: WDOperation, didProduce newOperation: Operation) {
        // No op.
    }

    public func operationDidFinish(_ operation: WDOperation, with errors: [NSError]) {
        // No op.
    }
}
