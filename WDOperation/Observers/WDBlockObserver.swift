/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This file shows how to implement the OperationObserver protocol.
*/

import Foundation

/**
    The `BlockObserver` is a way to attach arbitrary blocks to significant events
    in an `Operation`'s lifecycle.
*/
public struct WDBlockObserver: WDOperationObserver {
    // MARK: Properties
    
    private let startHandler: ((WDOperation) -> Void)?
    private let produceHandler: ((WDOperation, Operation) -> Void)?
    private let finishHandler: ((WDOperation, [NSError]) -> Void)?
    
    public init(startHandler: ((WDOperation) -> Void)? = nil, produceHandler: ((WDOperation, Operation) -> Void)? = nil, finishHandler: ((WDOperation, [NSError]) -> Void)? = nil) {
        self.startHandler = startHandler
        self.produceHandler = produceHandler
        self.finishHandler = finishHandler
    }
    
    // MARK: OperationObserver
    
    public func operationDidStart(_ operation: WDOperation) {
        startHandler?(operation)
    }
    
    public func operation(_ operation: WDOperation, didProduce newOperation: Operation) {
        produceHandler?(operation, newOperation)
    }
    
    public func operationDidFinish(_ operation: WDOperation, with errors: [NSError]) {
        finishHandler?(operation, errors)
    }
}
