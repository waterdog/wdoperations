/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This file shows an example of implementing the OperationCondition protocol.
*/

import EventKit
import WDOperations

/// A condition for verifying access to the user's calendar.
public struct WDCalendarCondition: WDOperationCondition {
    
    public static let name = "Calendar"
    public static let entityTypeKey = "EKEntityType"
    public static let isMutuallyExclusive = false
    
    public let entityType: EKEntityType
    
    public init(entityType: EKEntityType) {
        self.entityType = entityType
    }
    
    public func dependencyForOperation(_ operation: WDOperation) -> Operation? {
        return CalendarPermissionOperation(entityType: entityType)
    }
    
    public func evaluateForOperation(_ operation: WDOperation, completion: @escaping (WDOperationConditionResult) -> Void) {
        switch EKEventStore.authorizationStatus(for: entityType) {
            case .authorized:
                completion(.satisfied)

            default:
                // We are not authorized to access entities of this type.
                let error = NSError(code: .conditionFailed, userInfo: [
                    WDOperationConditionKey: type(of: self).name,
                    type(of: self).entityTypeKey: entityType.rawValue
                ])
                
                completion(.failed(error))
        }
    }
}

/**
    `EKEventStore` takes a while to initialize, so we should create
    one and then keep it around for future use, instead of creating
    a new one every time a `CalendarPermissionOperation` runs.
*/
private let SharedEventStore = EKEventStore()

/**
    A private `Operation` that will request access to the user's Calendar/Reminders,
    if it has not already been granted.
*/
private class CalendarPermissionOperation: WDOperation {
    let entityType: EKEntityType
    
    init(entityType: EKEntityType) {
        self.entityType = entityType
        super.init()
        addCondition(WDAlertPresentation())
    }
    
    override func execute() {
        let status = EKEventStore.authorizationStatus(for: entityType)
        
        switch status {
        case .notDetermined:
            DispatchQueue.main.async {
                SharedEventStore.requestAccess(to: self.entityType) { granted, error in
                    self.finish()
                }
            }
            
        default:
            finish()
        }
    }
    
}
