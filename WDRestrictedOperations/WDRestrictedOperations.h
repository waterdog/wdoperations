//
//  WDRestrictedOperations.h
//  WDRestrictedOperations
//
//  Created by Jorge Galrito on 12/04/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WDRestrictedOperations.
FOUNDATION_EXPORT double WDRestrictedOperationsVersionNumber;

//! Project version string for WDRestrictedOperations.
FOUNDATION_EXPORT const unsigned char WDRestrictedOperationsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WDRestrictedOperations/PublicHeader.h>


